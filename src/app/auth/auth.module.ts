import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { AngularFireAuth } from '@angular/fire/auth';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material';
import { SessionButtonsComponent } from './components/session-buttons/session-buttons.component';


@NgModule({
  declarations: [LoginFormComponent, SessionButtonsComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    ReactiveFormsModule,
    MatSnackBarModule
  ],
  providers: [
    AngularFireAuth
  ],
  exports: [
    SessionButtonsComponent
  ]
})
export class AuthModule { }
