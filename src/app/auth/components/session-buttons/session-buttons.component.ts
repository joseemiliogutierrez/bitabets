import { Component, OnInit } from '@angular/core';
import { AuthService } from '@auth/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-session-buttons',
  templateUrl: './session-buttons.component.html',
  styleUrls: ['./session-buttons.component.scss']
})
export class SessionButtonsComponent implements OnInit {

  session;
  user;

  constructor(private authService: AuthService, private router: Router) {
    this.hasSession();
  }

  ngOnInit() {
  }

  hasSession() {
    this.authService.hasSession().subscribe(session => {
      this.session = session;
    });
  }

  logout() {
    this.authService.logout().then(() => {
      this.router.navigate(['']);
    });
  }

}
