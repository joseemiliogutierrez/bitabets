import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class AngularMaterialService {

  constructor(private snack: MatSnackBar) { }

  showMessage(message: string, classStyle: string) {
    this.snack.open(message, '', {
      duration: 4000,
      verticalPosition: 'top',
      horizontalPosition: 'left',
      panelClass: [classStyle]
    });
  }
}
